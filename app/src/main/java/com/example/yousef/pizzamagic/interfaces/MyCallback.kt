package com.example.yousef.pizzamagic.interfaces

import com.example.yousef.pizzamagic.models.Product

/**
 * Created by Yousef on 7/18/2018.
 */
interface MyCallback {
    fun onCallback(products: List<Product>?)
}