package com.example.yousef.pizzamagic.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.view.Menu
import android.view.MenuItem
import com.example.yousef.pizzamagic.R
import com.example.yousef.pizzamagic.adapters.CategoriesListAdapter
import com.example.yousef.pizzamagic.enums.Categories
import com.example.yousef.pizzamagic.enums.IntentExtras
import com.example.yousef.pizzamagic.models.OrdersSingelton
import kotlinx.android.synthetic.main.activity_categories.*
import org.jetbrains.anko.toast

class CategoriesActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_categories)

        //Setting an adapter to categories list
        categories_list.adapter = CategoriesListAdapter(this, resources.obtainTypedArray(R.array.categories_imgs))

        //Setting listener to choose from categories list
        categories_list.setOnItemClickListener{_, _, position, _ -> goToProducts(position)}
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            R.id.check -> {
                goToSendOrder()
                return true
            } R.id.delete -> {
            deleteOrder()
            return true
        }
        }
        return super.onOptionsItemSelected(item)
    }

    //Show alert before deleting
    private fun deleteOrder() {
        AlertDialog.Builder(this).setTitle("الغاء الطلب").setMessage("حذف جميع العناصر؟")
                .setPositiveButton("حذف", { dialog, _ ->
                    OrdersSingelton.instance.orderItems.clear()
                    dialog.dismiss()
                    toast("تم حذف الطلبات")
                })
                .setNegativeButton("تراجع", { dialog, _ ->
                    dialog.dismiss()
                })
                .setCancelable(true)
                .show()
    }

    //Determine which category is pressed (position) and put it as an intent extra
    private fun goToProducts(position: Int) {

        val intent = Intent(this, ProductsActivity::class.java)

        when (position){
            Categories.PIZZA.position -> intent.putExtra(IntentExtras.CATEGORY.name, Categories.PIZZA)
            Categories.PIES.position -> intent.putExtra(IntentExtras.CATEGORY.name, Categories.PIES)
            Categories.CREPES.position -> intent.putExtra(IntentExtras.CATEGORY.name, Categories.CREPES)
            Categories.PASTA.position -> intent.putExtra(IntentExtras.CATEGORY.name, Categories.PASTA)
        }

        startActivity(intent)
    }

    private fun goToSendOrder(){
        if (OrdersSingelton.instance.orderItems.size == 0) {
            toast("لا يوجد طلبات للارسال!")
            return
        }
        startActivity(Intent(this, SendOrderActivity::class.java))
    }
}
