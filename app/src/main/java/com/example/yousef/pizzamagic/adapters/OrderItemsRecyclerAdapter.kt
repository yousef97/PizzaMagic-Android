package com.example.yousef.pizzamagic.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.TextView
import com.example.yousef.pizzamagic.R
import com.example.yousef.pizzamagic.interfaces.MyCallback
import com.example.yousef.pizzamagic.models.OrdersSingelton

/**
 * @context is the context of the invoking activity to inflate views.
 * @orderItems is the keys of the order items as list to access it by index, display it and manipulate it.
 * @callback to notify the activity if items is changed to update the total price
 * Created by Yousef on 7/19/2018.
 */
class OrderItemsRecyclerAdapter(private val context: Context, private val orderItems: MutableList<String>,
                                private val callback: MyCallback): RecyclerView.Adapter<OrderItemViewHolder>(){

    override fun getItemCount(): Int {
        return OrdersSingelton.instance.orderItems.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): OrderItemViewHolder {
        return OrderItemViewHolder(LayoutInflater.from(context).inflate(R.layout.row_order_item, parent, false))

    }

    //Populating data to views, attaching click listeners
    override fun onBindViewHolder(holder: OrderItemViewHolder?, position: Int) {
        holder!!.num!!.text = OrdersSingelton.instance.orderItems[orderItems[position]]!!.num.toString()
        holder!!.name!!.text = orderItems[position]
        holder!!.price!!.text = "${(OrdersSingelton.instance.orderItems[orderItems[position]]!!.num *
                                    OrdersSingelton.instance.orderItems[orderItems[position]]!!.unitPrice)} ر.س "

        holder!!.addBtn!!.setOnClickListener{ _ -> addBtnPressed(orderItems[position], position)}
        holder!!.minusBtn!!.setOnClickListener{ _ -> minusBtnPressed(orderItems[position], position) }
    }

    //When add button is pressed it increments the value of the specified key by one and notifies the activity
    private fun addBtnPressed(key: String, position: Int){
        OrdersSingelton.instance.orderItems[key]!!.num += 1
        notifyItemChanged(position)
        callback.onCallback(null)
    }

    //When minus button is pressed it decrements the value of the specified key by one, if it is 1 it will be deleted,
    //then notifies the activity anyway
    private fun minusBtnPressed(key: String, position: Int){
        if (OrdersSingelton.instance.orderItems[key]!!.num == 1){
            OrdersSingelton.instance.orderItems.remove(key)
            orderItems.removeAt(position)
            notifyDataSetChanged()
            callback.onCallback(null)
            return
        }

        OrdersSingelton.instance.orderItems[key]!!.num -= 1
        notifyItemChanged(position)
        callback.onCallback(null)
    }


}

//Class holds views to populate it with data in onBindViewHolder method
class OrderItemViewHolder(view: View): RecyclerView.ViewHolder(view){
    var addBtn: ImageButton? = null
    var minusBtn: ImageButton? = null
    var num: TextView? = null
    var name: TextView? = null
    var price: TextView? = null
    init {
        addBtn = view.findViewById(R.id.plus_item_btn)
        minusBtn = view.findViewById(R.id.minus_item_btn)
        num = view.findViewById(R.id.item_num)
        name = view.findViewById(R.id.item_name)
        price = view.findViewById(R.id.price)
    }
}