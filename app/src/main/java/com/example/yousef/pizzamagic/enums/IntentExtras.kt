package com.example.yousef.pizzamagic.enums

/**
 * Created by Yousef on 7/16/2018.
 */
enum class IntentExtras(val constantName: String){
    CATEGORY("category")
}