package com.example.yousef.pizzamagic.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import com.example.yousef.pizzamagic.R
import com.example.yousef.pizzamagic.adapters.ProductsRecyclerAdapter
import com.example.yousef.pizzamagic.enums.Categories
import com.example.yousef.pizzamagic.enums.IntentExtras
import com.example.yousef.pizzamagic.interfaces.MyCallback
import com.example.yousef.pizzamagic.models.OrdersSingelton
import com.example.yousef.pizzamagic.models.Product
import com.example.yousef.pizzamagic.networking.FireBaseUtils
import kotlinx.android.synthetic.main.activity_products.*
import org.jetbrains.anko.toast


class ProductsActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_products)

        //Getting category sent as intent extra
        val category = intent.extras.get(IntentExtras.CATEGORY.name) as Categories

        //Implementing DataCallBack interface to invoke onCallBack method once data is received to setup list
        val onCallBack = object : MyCallback {
            override fun onCallback(products: List<Product>?) {
                setupList(category, products!!)
            }
        }

        //Getting products of the specified category
        FireBaseUtils.getProducts(category.firebaseName, onCallBack)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu, menu)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when(item!!.itemId){
            R.id.check -> {
                goToSendOrder()
                return true
            } R.id.delete -> {
              deleteOrder()
              return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    //Show alert before deleting
    private fun deleteOrder() {
        AlertDialog.Builder(this).setTitle("الغاء الطلب").setMessage("حذف جميع العناصر؟")
                .setPositiveButton("حذف", { dialog, _ ->
                    OrdersSingelton.instance.orderItems.clear()
                    dialog.dismiss()
                    toast("تم حذف الطلبات")
                })
                .setNegativeButton("تراجع", { dialog, _ ->
                    dialog.dismiss()
                })
                .setCancelable(true)
                .show()
    }

    //Sets layout manager and adapter to products list to populate it with data
    private fun setupList(category: Categories,products: List<Product>) {
        products_recycler.layoutManager = LinearLayoutManager(this)
        products_recycler.adapter = ProductsRecyclerAdapter(this, category, products)
    }

    private fun goToSendOrder(){
        if (OrdersSingelton.instance.orderItems.size == 0) {
            toast("لا يوجد طلبات للارسال!")
            return
        }
        startActivity(Intent(this, SendOrderActivity::class.java))
    }
}
