package com.example.yousef.pizzamagic.models

/**
 * Created by Yousef on 7/16/2018.
 */
data class Product(val name: String = "", val smallPrice: Int = 0, val mediumPrice:Int = 0, val largePrice: Int = 0)