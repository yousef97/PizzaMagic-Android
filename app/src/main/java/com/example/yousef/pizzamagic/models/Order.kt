package com.example.yousef.pizzamagic.models

/**
 * Created by Yousef on 7/18/2018.
 */
data class Order(val items: LinkedHashMap<String, OrderItem>, val name: String, val phoneNum: String, val address: String, val comments: String)