package com.example.yousef.pizzamagic.models

/**
 * Created by Yousef on 7/16/2018.
 */
class OrdersSingelton private constructor(){

    var orderItems = LinkedHashMap<String, OrderItem>()

    fun getTotalPrice(): Int{
        var totalPrice = 0
        for ((_, value) in orderItems){
            totalPrice += value.num * value.unitPrice
        }
        return totalPrice
    }

    //Holder object& lazy instance is used to ensure only one instance of Singleton is created
    private object Holder { val INSTANCE = OrdersSingelton() }

    companion object {
        val instance: OrdersSingelton by lazy { Holder.INSTANCE }
    }
}