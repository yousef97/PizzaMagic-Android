package com.example.yousef.pizzamagic.networking

import android.util.Log
import com.example.yousef.pizzamagic.interfaces.MyCallback
import com.example.yousef.pizzamagic.models.Order
import com.example.yousef.pizzamagic.models.Product
import com.google.firebase.database.*

/**
 * Created by Yousef on 7/18/2018.
 */
class FireBaseUtils{

    //Static methods
    companion object {
        fun getProducts(category: String, callback: MyCallback) {
            val type = object : GenericTypeIndicator<ArrayList<Product>>() {} //The type the incoming data will be casted to

            //Get the data from firebase and notify activity that data is received via a callback
            FirebaseDatabase.getInstance().getReference(category)
                    .addListenerForSingleValueEvent(object : ValueEventListener {
                        override fun onDataChange(p0: DataSnapshot?) {
                            val products = p0!!.getValue(type) as ArrayList<Product>
                            callback.onCallback(products)
                        }

                        override fun onCancelled(p0: DatabaseError?) {
                            Log.e("debug", p0.toString())
                        }
                    })
        }

        fun sendOrder(order: Order){
            FirebaseDatabase.getInstance().getReference("Orders").push().setValue(order)
        }
    }
}