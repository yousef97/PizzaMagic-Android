package com.example.yousef.pizzamagic.adapters

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageButton
import android.widget.ImageView
import android.widget.TextView
import org.jetbrains.anko.toast
import com.example.yousef.pizzamagic.R
import com.example.yousef.pizzamagic.enums.Categories
import com.example.yousef.pizzamagic.enums.Sizes
import com.example.yousef.pizzamagic.models.OrderItem
import com.example.yousef.pizzamagic.models.OrdersSingelton
import com.example.yousef.pizzamagic.models.Product

/**
 * @context is the context of the invoking activity to push toasts and inflate views
 * @category is the category of food to push it to firebase
 * @products is the products will be viewed
 *
 * Created by Yousef on 7/16/2018.
 */
class ProductsRecyclerAdapter(private val context: Context, private val category: Categories,
                              private val products: List<Product>): RecyclerView.Adapter<ProductViewHolder>() {

    //Setting appropriate image for the selected category
    var image: Int
    init {
        when (category){
            Categories.PIZZA -> image = R.drawable.pizza_product
            Categories.PIES -> image = R.drawable.pie_product
            Categories.CREPES -> image = R.drawable.crepe_product
            Categories.PASTA -> image = R.drawable.pasta_product
        }
    }

    override fun getItemCount(): Int {
        return products.size
    }

    override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ProductViewHolder {
        return ProductViewHolder(LayoutInflater.from(context).inflate(R.layout.row_product, parent, false))
    }

    //Populating data to views, attaching click listeners
    override fun onBindViewHolder(holder: ProductViewHolder?, position: Int) {
        holder!!.productImage!!.setImageResource(image)
        holder!!.productName!!.text = products[position].name

        holder!!.largePrice!!.text = getSizeIfAvailable(holder.addLargeBtn!!, products[position].largePrice)
        holder!!.addLargeBtn!!.setOnClickListener{ _ -> addBtnPressed(position, category, Sizes.LARGE.arabicName)}

        holder!!.mediumPrice!!.text = getSizeIfAvailable(holder.addMediumBtn!!, products[position].mediumPrice)
        holder!!.addMediumBtn!!.setOnClickListener{ _ -> addBtnPressed(position, category, Sizes.MEDIUM.arabicName)}

        holder!!.smallPrice!!.text = getSizeIfAvailable(holder.addSmallBtn!!, products[position].smallPrice)
        holder!!.addSmallBtn!!.setOnClickListener{ _ -> addBtnPressed(position, category, Sizes.SMALL.arabicName)}
    }

    /*
    *Invoked when any add button is pressed, checks if the item is ordered before then increments it, if not pushes it
    *and assures the user by pushing a toast
    */
    private fun addBtnPressed(position: Int, category: Categories, size: String) {
        val key = "${category.arabicName} ${products[position].name} $size"
        var price = 0
        when (size){
            Sizes.SMALL.arabicName -> price = products[position].smallPrice
            Sizes.MEDIUM.arabicName -> price = products[position].mediumPrice
            Sizes.LARGE.arabicName -> price = products[position].largePrice
        }
        if (OrdersSingelton.instance.orderItems.containsKey(key))
            OrdersSingelton.instance.orderItems[key]!!.num += 1
        else
            OrdersSingelton.instance.orderItems[key] = OrderItem(1, price)

        context.toast("لقد أضفت $key")
    }

    /*
    *Returns the size of the related item and size, if this size not available returns "--" and disables
    *related button and makes it grey
    */
    private fun getSizeIfAvailable(button: ImageButton, price: Int): String{
        if (price == 0){
            button.isEnabled = false
            button.setImageResource(R.drawable.ic_add_circle_outline_grey_24dp)
            return "--"
        }
        return price.toString()
    }
}

//Class holds views to populate it with data in onBindViewHolder method
class ProductViewHolder(view: View): RecyclerView.ViewHolder(view) {
    var productImage: ImageView? = null
    var productName: TextView? = null
    var largePrice: TextView? = null
    var mediumPrice: TextView? = null
    var smallPrice: TextView? = null
    var addLargeBtn: ImageButton? = null
    var addMediumBtn: ImageButton? = null
    var addSmallBtn: ImageButton? = null

    init {
        productImage = view.findViewById(R.id.product_img)
        productName = view.findViewById(R.id.product_name)
        largePrice = view.findViewById(R.id.large_price)
        mediumPrice = view.findViewById(R.id.medium_price)
        smallPrice = view.findViewById(R.id.small_price)
        addLargeBtn = view.findViewById(R.id.add_large_btn)
        addMediumBtn = view.findViewById(R.id.add_medium_btn)
        addSmallBtn = view.findViewById(R.id.add_small_btn)
    }
}