package com.example.yousef.pizzamagic.activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import com.example.yousef.pizzamagic.R
import com.example.yousef.pizzamagic.adapters.OrderItemsRecyclerAdapter
import com.example.yousef.pizzamagic.interfaces.MyCallback
import com.example.yousef.pizzamagic.models.Order
import com.example.yousef.pizzamagic.models.OrdersSingelton
import com.example.yousef.pizzamagic.models.Product
import com.example.yousef.pizzamagic.networking.FireBaseUtils
import kotlinx.android.synthetic.main.activity_send_order.*
import org.jetbrains.anko.longToast
import org.jetbrains.anko.toast

class SendOrderActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_send_order)

        //Displaying Total Price
        updatePrice()

        //Implementing MyCallBack interface to invoke onCallBack method to update price once order is changed
        val updatePriceCallBack = object : MyCallback {
            override fun onCallback(products: List<Product>?) {
                updatePrice()
            }
        }

        //Setting adapter to the order items recycler
        val orderItemsKeys = ArrayList<String>(OrdersSingelton.instance.orderItems.keys)
        order_items_recycler.layoutManager = LinearLayoutManager(this)
        order_items_recycler.adapter = OrderItemsRecyclerAdapter(this, orderItemsKeys, updatePriceCallBack)

        //Setting a listener on send order button to invoke sendOrderBtnPressed() method when clicked
        send_order_btn.setOnClickListener{ _ -> sendOrderBtnPressed() }
    }

    //When send order button is pressed checks if any field is empty, if not send the order.
    private fun sendOrderBtnPressed() {
        val name = name_edt.text.toString()
        val phone = phone_edt.text.toString()
        val address = address_edt.text.toString()
        val comments = comments_edt.text.toString()

        //Check if user deleted all items
        if (OrdersSingelton.instance.orderItems.size == 0) {
            toast("لا يوجد طلبات للارسال!")
            return
        }

        //Check for empty needed info
        if (name == "" || phone == "" || address == "") {
            toast("أدخل البيانات!")
            return
        }

        //Packaging order and sending it
        val order = Order(OrdersSingelton.instance.orderItems, name, phone, address, comments)
        FireBaseUtils.sendOrder(order)
        longToast("تم ارسال الطلب..سيصلك خلال وقت قصير")

        //Return to the main screen clearing any opened activity and clearing order
        val mIntent = Intent(this, CategoriesActivity::class.java)
        mIntent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP
        startActivity(mIntent)
        OrdersSingelton.instance.orderItems.clear()

    }

    //Displays the total price of order
    private fun updatePrice() {
        total_price.text = "${OrdersSingelton.instance.getTotalPrice()} ر.س "
    }
}
