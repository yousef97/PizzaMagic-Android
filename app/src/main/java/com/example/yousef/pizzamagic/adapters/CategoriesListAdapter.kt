package com.example.yousef.pizzamagic.adapters

import android.app.Activity
import android.content.Context
import android.content.res.TypedArray
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.ImageView
import com.example.yousef.pizzamagic.R

/**
 * Created by Yousef on 7/16/2018.
 */
class CategoriesListAdapter(private val activity: Activity, private val images: TypedArray): BaseAdapter() {

    //Inner private class to hold views for rows
    private class ViewHolder(row: View?) {
        var categoryImg: ImageView? = null

        init {
            categoryImg = row!!.findViewById(R.id.category_image)
        }
    }

    //Return the required cell
    override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {
        val view: View?
        val viewHolder: ViewHolder
        if (convertView == null) {
            val inflater = activity?.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            view = inflater.inflate(R.layout.row_category, null)
            viewHolder = ViewHolder(view)
            view?.tag = viewHolder
        } else {
            view = convertView
            viewHolder = view.tag as ViewHolder
        }

        viewHolder.categoryImg?.setImageResource(images.getResourceId(position, -1))
        return view as View
    }

    //Return the data object will be used
    override fun getItem(position: Int): Any {
        return images.getResourceId(position, -1)
    }

    //Return Item ID
    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    //Return the size of array
    override fun getCount(): Int {
        return images.length()
    }

}