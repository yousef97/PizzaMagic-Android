package com.example.yousef.pizzamagic.enums

/**
 * Created by Yousef on 7/16/2018.
 */
enum class Sizes(val arabicName: String){
    SMALL("صغير"), MEDIUM("وسط"), LARGE("كبير")
}