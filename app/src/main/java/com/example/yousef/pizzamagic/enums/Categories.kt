package com.example.yousef.pizzamagic.enums

/**
 * Created by Yousef on 7/16/2018.
 */

enum class Categories(val position: Int, val arabicName: String, val firebaseName: String){
    PIZZA (0, "بيتزا", "Pizza"), PIES (1, "فطير", "Pies"), CREPES(2, "كريب", "Crepes"), PASTA(3, "باستا", "Pasta")
}