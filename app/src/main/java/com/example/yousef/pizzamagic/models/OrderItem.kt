package com.example.yousef.pizzamagic.models

/**
 * @num: number of items of this item
 * @unitPrice: unit price of this specific item and size
 * Created by Yousef on 7/21/2018.
 */
data class OrderItem(var num: Int, val unitPrice: Int)